# Proof of concept: using openssl from Go

The idea is to have the same binary what can work in both FIPS and non-FIPS compliant environments without modifications
We'll use the following open source package:

    https://pkg.go.dev/github.com/spacemonkeygo/openssl

Numerous forks of the above package support FreeBSD and Solaris, but we don't really care about these. If the forks have
extensions we need, we can take them as well, and maintain our own package. Right now, we don't have to. The package is
published under Apache License 2.0.

## Building for your platform

The Go - openssl binding supports the following platforms

* Linux
* macOS
* Windows

dependencies installation is described here:

    https://pkg.go.dev/github.com/spacemonkeygo/openssl

## Quick start

Quick start is supported by `docker` build, in docker directory. To build an amd64 binary compatible with Linux and
docker, take the following steps, assuming the project is in ~/projects/useopenssl/ :

* cd ~/projects/useopenssl/docker
* docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) -f ./Dockerfile -t gollum:0.0.2 ./resources
* docker run -it --rm -v ${PWD}/out:/out gollum:0.0.2

After successful completion of the steps above, a binary compatible with Ubuntu 18.04 and openssl 1.1 will be found in
~/projects/useopenssl/docker/out directory. The executable name is `gollum`

If your build environment is Ubuntu 18, you're most likely ready to run the executable. If not, you need to copy it to a
compatible VM or docker container together with certificate and key used to create a simple https server. In my case, I
copy them to a directory which is shared with vagrant VM:

* cd ~/projects/vagrantVM/
* cp ~/projects/useopenssl/docker/out/gollum .
* cp ~/projects/useopenssl/my_server.* .

## Running the executable

In vagrant VM, run as following:

* cd /vagrant/
* ./gollum

The output on FIPS-certified machine will be like this:

    vagrant@vm:/vagrant$ ./gollum
    Let's try MD5
    MD5 error: openssl: md5: cannot init digest ctx
    Now, let's start https server...

The MD5 hash is not enabled in FIPS, and the call returns error shown in the output.

In order to check the https server, first let's put it in the background:

    Now, let's start https server...
    ^Z
    [1]+  Stopped                 ./gollum
    vagrant@vm:/vagrant$ bg
    [1]+ ./gollum &
    vagrant@vm:/vagrant$

Now let's use curl to access the server to see headers:

    vagrant@vm:/vagrant$ curl https://localhost:8443/
    User-Agent: curl/7.58.0
    Accept: */*
    vagrant@vm:/vagrant$

Run the above command again with -v flag to see TLS handshake.

## Building for local platform

You can build it on local platform, and run it if the platform has openssl installed. Again, see instructions here:

    https://pkg.go.dev/github.com/spacemonkeygo/openssl

For macOS, I already have openssl installed, so I will run two commands, from the root of the project:

* source setenv.sh
* go build

If necessary, modify setenv.sh to provide Go SDK installation path, specified i by `GOROOT`

if the above commands were successful, you can run the executable directly:

    ./gollum
    ...
    Let's try MD5
    MD5 hash: 098f6bcd4621d373cade4e832627b4f6
    Now, let's start https server...

As you can see, the macOS version is not FIPS-compliant because it successfully computed MD5 hash. Follow the
instructions above to exercise https server functionality.







