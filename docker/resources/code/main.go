package main

import (
	"fmt"
	"github.com/spacemonkeygo/openssl"
	"log"
	"net/http"
)

func hello(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "hello\n")
}

func headers(w http.ResponseWriter, req *http.Request) {
	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
}

func main() {
	fmt.Printf("Let's try MD5\n")
	md5, err := openssl.MD5([]byte("test"))
	if err != nil {
		fmt.Printf("MD5 error: %s\n", err)
	} else {
		fmt.Printf("MD5 hash: %x\n", md5)
	}

	fmt.Printf("Now, let's start https server...\n")
	http.HandleFunc("/hello", hello)
	http.HandleFunc("/", headers)

	log.Fatal(openssl.ListenAndServeTLS(
		":8443", "my_server.crt", "my_server.key", nil))
}
